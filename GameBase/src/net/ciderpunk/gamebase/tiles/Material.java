package net.ciderpunk.gamebase.tiles;



public class Material {
	protected String name;
	protected boolean defaultMaterial;
	protected Tile[] tiles;
	private TileSet parent;
	
	public boolean isDefaultMaterial() {
		return defaultMaterial;
	}

	public String getName() {
		return this.name;
	}

	public void initTiles(TileSet ts) {
		parent = ts;
		for(Tile tile : tiles){
			tile.initTile(ts);
		}
	}
	
	
	public Tile getTile(int num){
		if (num < tiles.length){
			return tiles[num];
		}
		return tiles[0];
	}
	
	
	public Tile getDefaultTile(){ 
		return tiles[0];
	}


	
}
