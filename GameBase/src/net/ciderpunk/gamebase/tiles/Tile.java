package net.ciderpunk.gamebase.tiles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import net.ciderpunk.gamebase.gfx.Frame;

public class Tile{
	public Material material;
	protected int x,y;
	Frame frame;
	
	
	public void initTile(TileSet ts) {		
		frame = new Frame(new TextureRegion(ts.getTexture(), x * ts.getTileWidth(), y * ts.getTileHeight(), ts.getTileHeight(), ts.getTileHeight() ), 0, 0);
	}
		
	public Material getMaterial()
	{
		return this.material; 
	}
	
	public void draw(SpriteBatch batch, float x, float y){
		frame.draw(batch, x, y);
	}
	
	
}
