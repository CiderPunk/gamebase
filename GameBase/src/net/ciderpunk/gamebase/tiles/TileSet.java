package net.ciderpunk.gamebase.tiles;

import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.ObjectMap;

public class TileSet  implements IResourceUser{

	

	protected String name;
	protected String texturePath;
	protected Material[] materials;
	protected Texture tileTexture; 
	protected int tileWidth,tileHeight;
	protected Material defaultMaterial;
	private ObjectMap<String, Material> materialMap;
	
	
	public TileSet(){
		this.materials = null;
		this.name = "";
		this.texturePath = "";
		this.tileWidth = 0; 
		this.tileHeight=0;
		this.tileTexture = null;
	}
	
	
	public static TileSet LoadTileset(String path){
		Json json = new Json();
		FileHandle file = Gdx.files.internal(path);
		String text = file.readString();
		return json.fromJson(TileSet.class, text);		
	}


	@Override
	public void preLoad(ResourceManager resMan) {
		resMan.getAssetMan().load(texturePath, Texture.class);
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		materialMap = new ObjectMap<String,Material>( materials.length );
		tileTexture = resMan.getAssetMan().get(this.texturePath, Texture.class);
		for(Material m : materials){
			materialMap.put(m.getName(), m);
			defaultMaterial = m.isDefaultMaterial() ? m : defaultMaterial; 
			m.initTiles(this);
		}
	}

	public Material getMaterial(String name){
		return materialMap.get(name);
	}

	public Material getDefaultMaterial(){
		return this.defaultMaterial;
	}

	
	
	public Texture getTexture(){return this.tileTexture; } 
	public int getTileHeight(){return this.tileHeight;}
	public int getTileWidth(){return this.tileWidth;}
	
}
