package net.ciderpunk.gamebase.gui;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Json;

import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;

/**
 * @author Matthew
 *
 */
public class Theme implements IResourceUser {

	
	public static Theme LoadTheme(String path){
		Json json = new Json();
		FileHandle file = Gdx.files.internal(path);
		String text = file.readString();
		return json.fromJson(Theme.class, text);		
	}
	
	
	protected Theme[] children;
	protected String name;
	protected Theme parent;
	
	
	protected int topPad;
	protected int leftPad;
	protected int rightPad;
	protected int bottomPad;
	protected String patchPath;
	protected String fontPath;
	protected String atlasPath;
	protected String TexturePath;

	protected Object atlas;
	protected Object patch;
	protected Object font;
	protected Object texture;
	
	/*
	protected TextureAtlas atlas;
	protected NinePatch patch;
	protected BitmapFont font;
*/
	
	public Theme(){
		this.children = null;
		this.topPad = this.leftPad = this.rightPad = this.bottomPad = 0;
		this.atlas = null;
		this.patch = null;
		this.font = null;
		this.patchPath = this.fontPath = this.atlasPath = null;
	}
	
	public String getName() {
		return name;
	}

	public int getTopPad() {
		return topPad;
	}

	public int getLeftPad() {
		return leftPad;
	}
	
	public int getRightPad() {
		return rightPad;
	}

	public int getBottomPad() {
		return bottomPad;
	}

	public TextureAtlas getAtlas() {
		return (TextureAtlas)( this.atlas == null ? this.atlas = this.parent.getAtlas() : this.atlas);
	}

	public NinePatch getPatch() {
		return (NinePatch)(this.patch == null ? this.patch = this.parent.getPatch() : this.patch);
	}

	public BitmapFont getFont() {
		return (BitmapFont)(this.font == null ? this.font = this.parent.getFont() : this.font);
	}
	public TextureRegion getTexture(){
		return (TextureRegion)(this.texture == null ? this.texture = this.parent.getTexture() : this.texture);
	}
	

	@Override
	public void preLoad(ResourceManager resMan) {
		AssetManager assMan = resMan.getAssetMan();
		if (this.fontPath != null){
			assMan.load(this.fontPath, BitmapFont.class);
		}
		if (this.atlasPath != null){
			assMan.load(this.atlasPath, TextureAtlas.class);
		}
		if (this.children!= null){
			for (Theme child : children){
				child.parent = this;
				resMan.addResourceUser(child);
			}
		}
	}

	@Override
	public void postLoad(ResourceManager resMan) {
		AssetManager assMan = resMan.getAssetMan();
		if (this.fontPath != null){
			this.font = assMan.get(this.fontPath, BitmapFont.class);
		}
		if (this.atlasPath != null){
			this.atlas = assMan.get(this.atlasPath, TextureAtlas.class);
		}
		if (this.patchPath != null){
			this.patch = this.getAtlas().createPatch(this.patchPath);
		}
		if (this.TexturePath!= null){
			this.patch = this.getAtlas().findRegion(this.TexturePath);
		}
		
	}
	
	/**
	 * gets the theme node by path
	 * @param path
	 * @return
	 */
	public Theme getTheme(String path){
		String nodes[] = path.split("/");
		Theme current = this;
		for(String node : nodes){
			if (!node.isEmpty()){
				current = current.getChild(node);
			}
		}
		return current;
	}
	
	
	protected Theme getChild(String pathNode){
		if (this.children != null){
			for (Theme child : this.children){
				if (child.getName().equals(pathNode)){
					return child;
				}
			}
		}
		return null;
	}
	

}
