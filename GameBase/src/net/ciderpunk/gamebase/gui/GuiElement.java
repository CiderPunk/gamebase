package net.ciderpunk.gamebase.gui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.utils.Array;
import net.ciderpunk.gamebase.input.events.InputEvent;
import net.ciderpunk.gamebase.input.events.MouseEvent;
import net.ciderpunk.gamebase.input.events.TouchEvent;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;


import com.badlogic.gdx.graphics.g2d.SpriteBatch;
public abstract  class GuiElement implements IResourceUser {

	@Override
	public void preLoad(ResourceManager resMan) {
		if (children != null){
			for (GuiElement child : this.children){
				resMan.addResourceUser(child);				
			}
		}
	}

	@Override
	public void postLoad(ResourceManager resMan) {
	}

	public enum VerticalPosition{ Top, Bottom, Mid }
	public enum HorizontalPosition{ Left, Right, Mid }

	protected VerticalPosition yAnchor;
	protected HorizontalPosition xAnchor;
	protected int width,height;
	protected int xOffs, yOffs;
	protected int xPos, yPos;
	protected Array<GuiElement> children = null;
	protected GuiElement parent;
	protected boolean visible;
	
	
	public void addTopChild(GuiElement element){
		if (children == null){
			children = new Array<GuiElement>();
		}
		children.add(element);
		element.attach(this);
	}
	
	public void addChild(GuiElement element){
		if (children == null){
			children = new Array<GuiElement>();
		}
		children.add(element);
		element.attach(this);
	}
	
	public void removeChild(GuiElement element){
		if (children != null){
			children.removeValue(element, true);
		}
		element.detach();
		
	}
	
	public void attach( GuiElement newParent){
		this.parent = newParent;
		this.updatePosition();
		if (children!= null){
			for (GuiElement child : children){
				child.attach(this);
			}
		}
	}
	
	public void detach(){
		this.parent = null;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public GuiElement getRoot(){
		return (this.parent == null ? this : this.parent.getRoot());
	}
	
	public GuiElement getParent() {
		return parent;
	}

	public GuiElement(){
		this(0,0, HorizontalPosition.Left, VerticalPosition.Top,0,0);
	}
	
	public GuiElement(int w, int h, HorizontalPosition xAnch, VerticalPosition yAnch, int xOffset, int yOffset){
		this.parent = null;
		this.width = w; 
		this.height = h;
		this.xAnchor = xAnch;
		this.yAnchor = yAnch;
		this.xOffs = xOffset;
		this.yOffs = yOffset;
		this.visible= true;
	}
	
	public void resize(int width, int height){
		this.width = width;
		this.height = height;
		if (this.children != null){
			for(GuiElement child : this.children){
				child.updatePosition();
			}
		}
	}
	
	public void updatePosition(){
		if (parent  != null){
			switch(this.xAnchor){
				case Left:
					this.xPos = this.xOffs;
					break;
				case Right:
					this.xPos = parent.getWidth() - this.xOffs - this.width;;
					break;
				case Mid:
					this.xPos = (parent.getWidth() - this.width) / 2;
					break;
			}
			switch(this.yAnchor){
				case Bottom:
					this.yPos = this.yOffs;
					break;
				case Top:
					this.yPos = parent.getHeight() - this.yOffs -this.height;
					break;
				case Mid:
					this.yPos = (parent.getHeight() - this.height) / 2;
					break;
			}
		}
		if (this.children != null){
			for(GuiElement child : this.children){
				child.updatePosition();
			}
		}
	}
	
	public void draw(SpriteBatch batch, int xStart, int yStart){
		if (visible){
			doDraw(batch, xStart + this.xPos, yStart + this.yPos);
		}
	}
	
	
	protected void doDraw(SpriteBatch batch, int x, int y){
		drawChildren(batch, x, y);
	}
	
	public void drawChildren(SpriteBatch batch, int xstart, int ystart){
		if (children != null && children.size > 0){
			Iterator<GuiElement> i = children.iterator();
			while(i.hasNext()){
				i.next().draw(batch, xstart, ystart);
			}
		}
	}
	
	public void update(float dT){
		updateChildren(dT);	
	}
	
	public void updateChildren(float dT){
		if (children != null && children.size > 0){
			Iterator<GuiElement> i = children.iterator();
			while(i.hasNext()){
				i.next().update(dT);
			}
		}
	}

	public boolean getVisible(){ return this.visible;}
	public void setVisible(boolean value){ this.visible = value;}
	/**
	 * checks if the point is within the bounds of this element 
	 * @param x
	 * @param y
	 * @return true on contained
	 */
	protected Boolean contains(float x, float y){
		return (x > 0 && x < this.width && y > 0 && y < this.height);
	}
	
	public boolean inputEvent(InputEvent e, float x, float y){
		float xRel = x - this.xPos;
		float yRel = y - this.yPos;
		if (contains(xRel, yRel)){
			if (children!= null){
				for(GuiElement element : children){
					if (element.visible && element.inputEvent(e,xRel,yRel)){
						return true;
					}
				}
			}
			return e.doEvent(this, xRel, yRel);
		}
		return false;
	}
	
	public boolean onMouseMove(MouseEvent e, float xRel, float yRel) {
		//System.out.printf("MouseMove:  %s %f %f \n", this.getClass(), e.x, e.y );
		return false;
	}
	
	public boolean onTouch(TouchEvent e, float xRel, float yRel) {
		//System.out.printf("Touch:  %s %f %f %d %d\n", this.getClass(), e.x, e.y, e.pointer, e.button );
		return false;
	}

	public boolean onScroll(MouseEvent e) {
		//System.out.printf("Scroll: %s %d \n", this.getClass(), e.scroll );
		return false;
	}

	public boolean onDrag(TouchEvent e) {
		//System.out.printf("Drag: %s %f %f %d \n", this.getClass(), e.dX, e.dY, e.pointer );
		return false;
	}

	public boolean onRelease(TouchEvent e) {
		e.target = null;
		//System.out.printf("Release: %s %d \n", this.getClass(), e.pointer );
		return true;
	}
	
	public boolean onZoom(TouchEvent e, float zoom){
		//System.out.printf("Zoom: %s %f \n", this.getClass(), zoom);
		return true;
	}

	public int getxOffs() {
		return xOffs;
	}

	public void setxOffs(int xOffs) {
		this.xOffs = xOffs;
	}

	public int getyOffs() {
		return yOffs;
	}

	public void setyOffs(int yOffs) {
		this.yOffs = yOffs;
	}

	public int getxPos() {
		return xPos;
	}

	public int getyPos() {
		return yPos;
	}

	public VerticalPosition getyAnchor() {
		return yAnchor;
	}

	public void setyAnchor(VerticalPosition yAnchor) {
		this.yAnchor = yAnchor;
	}

	public HorizontalPosition getxAnchor() {
		return xAnchor;
	}

	public void setxAnchor(HorizontalPosition xAnchor) {
		this.xAnchor = xAnchor;
	}

}
