package net.ciderpunk.gamebase.gui;

import net.ciderpunk.gamebase.events.EventCaller;
import net.ciderpunk.gamebase.events.IEventCaller;
import net.ciderpunk.gamebase.events.IListener;
import net.ciderpunk.gamebase.input.events.TouchEvent;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Label extends ThemedElement  {
	Boolean fixedWidth, fixedHeight;
	String text;
	int textX, textY;
	
	
	public Label(HorizontalPosition xAnch, VerticalPosition yAnch, int xOffset, int yOffset, String text, Theme theme) {
		this(0,0,xAnch, yAnch, xOffset, yOffset, text, theme);
	}
	
	public Label(int w, int h, HorizontalPosition xAnch, VerticalPosition yAnch, int xOffset, int yOffset, String text, Theme theme) {
		super(w, h, xAnch, yAnch, xOffset, yOffset, theme);
		this.theme = theme.getTheme("label");
		this.text = text;
		refreshSize();
	}

	public void setText(String text)
	{
		this.text = text;
		refreshSize();
	}
	
	public String getText()
	{
		return this.text;
	}

	protected void refreshSize(){
		TextBounds bounds = theme.getFont().getBounds(this.text);
		this.height = (int) Math.ceil(theme.getFont().getLineHeight() + this.theme.getTopPad() + this.theme.getBottomPad());
		textY = (int) Math.ceil(this.height - this.theme.getTopPad());
		this.width = (int) Math.ceil(bounds.width + this.theme.getLeftPad() + this.theme.getRightPad());
		textX = theme.getLeftPad();
		this.updatePosition();
	}

	@Override
	public void doDraw(SpriteBatch batch, int x, int y) {
		theme.getFont().drawMultiLine(batch, this.getText(), x + textX, y + textY);
	}


}
