package net.ciderpunk.gamebase.gui;

import net.ciderpunk.gamebase.input.GameInputListener;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * @author Matthew
 *
 */
public abstract class GameScreen extends GuiElement implements IResourceUser{
	

	protected Camera camera;
	protected Viewport viewport;
	
	protected TextureAtlas atlas;
//	protected GameGestureListener inputListener;
	protected SpriteBatch batch;
	protected ResourceManager resMan;
	protected GameInputListener inputListener;
	
	protected final GameScreen self = this;
	
	public Camera getCamera() {
		return camera;
	}


	public GameInputListener getInputListener() {
		return inputListener;
	}

	
	
	public void clearScreen(){		
		Gdx.graphics.getGL20().glClearColor(0.1f, 0.6f, 0.1f, 0);
		Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	public GameScreen(int w, int h) {
		super(w, h, HorizontalPosition.Left, VerticalPosition.Top, 0, 0 );
		inputListener = new GameInputListener(this);
		Gdx.input.setInputProcessor(new InputMultiplexer(new GestureDetector(inputListener),inputListener));
		/*
		camera = new OrthographicCamera();
		viewport = new FitViewport(w, h, camera);
		*/
		
		viewport = new StretchViewport(w, h);
		camera = viewport.getCamera();
		batch = new SpriteBatch();
		resMan = new ResourceManager(this);
	}


	@Override
	public void updatePosition() {
		//do nothing, these buggers don't move
	}

	
	public void resize(int width, int height){
		
		viewport.update(width, height, true);
		//camera.setToOrtho(false, width, height);
		//super.resize(this.width, this.height);
	}
	
	public void dispose(){
		batch.dispose();
		resMan.dispose();
	}


	public void update(float dT){
		if (resMan.update()){
			this.draw(this.batch,0,0);
			this.think(dT);
			super.update(dT);
		}
	}
	
	abstract protected void think(float dT);
	
	public Vector2 unproject(Vector2 screenCoords){
		return viewport.unproject(screenCoords);
	}
	
	public Boolean checkBounds(Vector2 loc) {
		return this.checkBounds(loc, 20);
	}
	
	public Boolean checkBounds(Vector2 loc, int margin) {
		return loc.x > -margin && loc.x < this.width + margin && loc.y > -margin && loc.y < this.height+ margin;
	}
	
	public abstract void startGame();

}