package net.ciderpunk.gamebase.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;


/**
 * @author Matthew
 *
 */
public abstract class Entity {

	private GuiElement owner;


	/**
	 * constructor
	 * @param owner
	 */
	public Entity(GuiElement owner){
		this.owner = owner;
	}
	
	public Entity(){
		this(null);
	}
	
	public GuiElement getOwner() {
		return owner;
	}

	public Boolean canCollide(){
		return false;
	}

	public void dispose() {
 
	}

	public abstract Boolean update(float dT);
}
