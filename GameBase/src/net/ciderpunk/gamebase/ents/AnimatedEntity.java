package net.ciderpunk.gamebase.ents;

import net.ciderpunk.gamebase.gfx.AnimationBuilder;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public abstract class AnimatedEntity extends Entity2D {

	
	public AnimatedEntity() {
		super();
	}
	
	public AnimatedEntity(GuiElement owner, int x, int y) {
		super(owner, x, y);
	}


	public AnimatedEntity(GuiElement owner, Vector2 loc) {
		super(owner, loc);
	}


	protected float stateTime;
	protected Animation currentAnim;
	
	protected void setAnim(Animation anim){
		if (currentAnim != anim){
			stateTime = 0f;
			currentAnim = anim;
		}
	}
	
	@Override
	public Boolean update(float dT) {
		if (currentAnim != null){
			this.stateTime += dT;
			this.currentFrame = (Frame)currentAnim.getKeyFrame(this.stateTime, true);
		}
		return true;
	}

	
	/**
	 * builds an animation using the entire sequence as is
	 * @param atlas
	 * @param name
	 * @param frameDelay
	 * @param xOff
	 * @param yOff
	 * @return
	 */
	public Animation buildAnim(TextureAtlas atlas, String name, float frameDelay, int xOff, int yOff){
    return AnimationBuilder.buildAnim(atlas, name, frameDelay, xOff, yOff);
	}

	/**
	 * builds an animation with the specified sequence of frames
	 * @param atlas
	 * @param name
	 * @param frameDelay
	 * @param xOff
	 * @param yOff
	 * @param sequence
	 * @return
	 */
	public Animation buildAnim(TextureAtlas atlas, String name, float frameDelay, int xOff, int yOff, int[] sequence){
	  return AnimationBuilder.buildAnim(atlas, name, frameDelay, xOff, yOff, sequence);
	}
	
}
