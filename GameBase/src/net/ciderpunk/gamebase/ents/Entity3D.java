package net.ciderpunk.gamebase.ents;

import net.ciderpunk.gamebase.gui.GuiElement;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public abstract class Entity3D extends Entity {


	protected Vector3 loc;
	protected Vector3 angle;
	
	protected static Environment environment = null;
	
	protected ModelInstance modelInstance;
	 
	public Entity3D() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Entity3D(GuiElement owner) {
		super(owner);
		loc = new Vector3();
		angle = new Vector3();
		
	}
	
	public Entity3D(GuiElement owner, Vector3 loc){
		this(owner);
		this.loc.set(loc);
	}
	

	public void draw(ModelBatch batch){		
    batch.render(modelInstance,  environment);
		
	}

}
