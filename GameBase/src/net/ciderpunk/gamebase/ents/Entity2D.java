package net.ciderpunk.gamebase.ents;

import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.gui.GuiElement;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public abstract class Entity2D extends Entity {

	protected Vector2 loc;
	protected Frame currentFrame;
	static Vector2 distanceTest = new Vector2();
	
	
	public Entity2D() {
		super();

		// TODO Auto-generated constructor stub
	}


	public Entity2D(GuiElement owner, int x, int y){
		this(owner);
		this.loc.set(x,y);
	}
	
	public Entity2D(GuiElement owner, Vector2 loc){
		this(owner);
		this.loc.set(loc);
	}
	
	public Entity2D(GuiElement owner) {
		super(owner);
		loc = new Vector2(); 
	}

	public void draw(SpriteBatch batch){
		currentFrame.draw(batch, loc.x, loc.y);
	}

	
	public boolean pointCollisoonTest(Vector2 target, float distance) {
		float maxDist = distance + this.getCollisionRadius();
		distanceTest.set(this.loc).sub(target);
		if (Math.abs(distanceTest.x) < maxDist && Math.abs(distanceTest.y) < maxDist){
			return distanceTest.len2() < (maxDist * maxDist);
		}
		return false;
	}

	protected float getCollisionRadius() {
		return 0;
	}


	public Vector2 getLoc() {
		return loc;
	}



}
