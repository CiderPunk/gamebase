package net.ciderpunk.gamebase.ents;

import com.badlogic.gdx.utils.PooledLinkedList;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.math.Vector2;

public class EntityList {

	protected PooledLinkedList<Entity> mainBuffer;
	protected PooledLinkedList<Entity> addBuffer;

  public EntityList(int mainBufferSize, int addBufferSize) {
    addBuffer = new PooledLinkedList<Entity>(addBufferSize);
    mainBuffer = new PooledLinkedList<Entity>(mainBufferSize);
  }

	public EntityList() {
    this(1000,100);
	}

	public void clear(){
		addBuffer.clear();
		mainBuffer.clear();
	}
	
	public void consolidateList(){
    Entity ent;

		addBuffer.iter();
		while((ent  = addBuffer.next()) != null) {
			mainBuffer.add(ent);
      addBuffer.remove();
		}
	}

	/** Performs update on all members of the list
	 * @param dT delta time
	 */
	public EntityList doUpdate(float dT){
		this.consolidateList();
    Entity ent;
    mainBuffer.iter();
		while((ent = mainBuffer.next()) != null){
      if (!ent.update(dT)){
				mainBuffer.remove();
			}
		}
		return this;
	}

	/**
	 * perform draw operation on all members of the list
	 * @param batch
	 */
	public void doDraw(SpriteBatch batch){

    Entity ent;
    mainBuffer.iter();
    while((ent = mainBuffer.next()) != null){
      if (ent instanceof Entity2D){
        ((Entity2D)ent).draw(batch);
      }
    }
	}
	
	
	public void doDraw(ModelBatch batch){
    Entity ent;
    mainBuffer.iter();
    while((ent = mainBuffer.next()) != null){
      if (ent instanceof Entity3D){
        ((Entity3D)ent).draw(batch);
      }
    }
  }

	
	
	public Entity pointCollisonTest(Vector2 loc, float distance){
    Entity ent;
    mainBuffer.iter();
    while((ent = mainBuffer.next()) != null){
      if (ent instanceof Entity2D && ((Entity2D) ent).pointCollisoonTest(loc, distance)){
        return ent;
      }
    }
    return null;
  }

	
	public void add(Entity arg0) {
    addBuffer.add(arg0);
	}

}
