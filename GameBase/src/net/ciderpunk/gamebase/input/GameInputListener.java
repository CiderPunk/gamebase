package net.ciderpunk.gamebase.input;


import java.util.Iterator;

import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.input.events.InputEvent;
import net.ciderpunk.gamebase.input.events.MouseEvent;
import net.ciderpunk.gamebase.input.events.TouchEvent;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class GameInputListener implements InputProcessor, GestureListener {

	protected GameScreen game;
	protected static final int maxPointers = 10;	
	protected TouchEvent[] touch;
	protected MouseEvent mouse;
	TouchEvent captureEvent = null;
	Array<IKeyListener> keyNotifyList;
	
	private static Vector2 tempCoord = new Vector2();
	
	
	
	public GameInputListener(GameScreen game){
		this.game = game;
		//targets = new Array<GuiElement>();
		touch = new TouchEvent[maxPointers];
		for (int i = 0; i < maxPointers; i++){
			touch[i] = new TouchEvent(this, i);
		}
		mouse = new MouseEvent(this);
		keyNotifyList = new Array<IKeyListener>(true,16);
	}
	
	public GameScreen getGame(){
		return game;
	}

	@Override
	public boolean keyDown(int keycode) {
		//System.out.print("keyDown:" + keycode + "\n");
		
		Iterator<IKeyListener> i = keyNotifyList.iterator();
		while(i.hasNext()){
			if (i.next().keyDown(keycode)){
				break;
			}
		}		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		Iterator<IKeyListener> i = keyNotifyList.iterator();
		while(i.hasNext()){
			i.next().keyUp(keycode);
		}		
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		//unify button/pointer bs
		if(captureEvent== null){
			pointer = pointer == 0 ? button : pointer;
			TouchEvent e = touch[pointer];
			tempCoord.set(x,y);
			e.rawCoord.set(tempCoord);
			e.set(game.unproject(tempCoord), button);
			return game.inputEvent(e, e.coord.x, e.coord.y);
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		pointer = pointer == 0 ? button : pointer;
		return touch[pointer].doRelease();
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		return touch[pointer].doDrag(game.unproject(tempCoord.set(x,y)));	
	}

	@Override
	public boolean mouseMoved(int x, int y) {
		tempCoord.set(x,y);
		mouse.rawCoord.set(tempCoord);
		mouse.set(game.unproject(tempCoord));
		return game.inputEvent(mouse, mouse.coord.x, mouse.coord.y);
	}

	@Override
	public boolean scrolled(int amount) {
		mouse.doScroll(amount);
		return false;
	}

	public InputEvent getCaptureEvent() {
		return captureEvent;
	}

	public void setCaptureEvent(TouchEvent captureEvent) {
		this.captureEvent = captureEvent;
	}

	@Override
	public boolean fling(float arg0, float arg1, int arg2) {
		return false;
	}

	@Override
	public boolean longPress(float arg0, float arg1) {
		return false;
	}

	@Override
	public boolean pan(float arg0, float arg1, float arg2, float arg3) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 arg0, Vector2 arg1, Vector2 arg2, Vector2 arg3) {
		return false;
	}

	@Override
	public boolean tap(float arg0, float arg1, int arg2, int arg3) {
		return false;
	}

	@Override
	public boolean touchDown(float arg0, float arg1, int arg2, int arg3) {
		return false;
	}

	@Override
	public boolean zoom(float start, float current) {
		if(captureEvent!= null){
			return captureEvent.doZoom(start, current);
		}
		return false;
	}

	public void registerKeyListener(IKeyListener obj){
		if (!this.keyNotifyList.contains(obj, true)){
			this.keyNotifyList.insert(0, obj);
		}
	}

	public void unRegisterKeyListener(IKeyListener obj){
		this.keyNotifyList.removeValue(obj, true);
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
