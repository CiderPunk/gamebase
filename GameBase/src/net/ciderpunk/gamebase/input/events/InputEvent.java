package net.ciderpunk.gamebase.input.events;

import com.badlogic.gdx.math.Vector2;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.input.GameInputListener;

public abstract class InputEvent {
	
	public Vector2 rawCoord;
	public Vector2 coord;
	public GuiElement target = null;
	protected GameInputListener listener;
	
	public InputEvent(GameInputListener listener){
		this.listener = listener;
		coord = new Vector2();
		rawCoord = new Vector2();
	}

	public void set(Vector2 newCoord){
		coord.set(newCoord);
		target = null;
	}
	
	public void set(float x, float y){
		coord.x = x;
		coord.y = y;
		target = null;
	}
	
	public abstract boolean doEvent(GuiElement e, float xRel, float yRel);
		
}
