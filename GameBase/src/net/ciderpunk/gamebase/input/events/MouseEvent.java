package net.ciderpunk.gamebase.input.events;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.input.GameInputListener;

public class MouseEvent extends InputEvent {
	
	public MouseEvent(GameInputListener listener) {
		super(listener);
	}

	public int scroll;
	
	@Override
	public boolean doEvent(GuiElement e, float xRel, float yRel) {
		return e.onMouseMove(this,xRel,yRel);
	}

	public boolean doScroll(int amount) {
		scroll = amount;
		if (target != null){
			return target.onScroll(this);
		}
		return false;
	}

}
