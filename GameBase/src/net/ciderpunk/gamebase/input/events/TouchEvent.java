package net.ciderpunk.gamebase.input.events;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import net.ciderpunk.gamebase.gui.GuiElement;
import net.ciderpunk.gamebase.input.GameInputListener;

/**
 * @author Matthew
 *
 */
public class TouchEvent extends InputEvent {
	
	
	public static Vector2 temp = new Vector2();
	public int button, pointer;
	public Vector2 lastMove;
	public Vector2 offset;
	public float prevDist;
	protected long timeStamp;
	public long touchTime;
	
	
	public TouchEvent(GameInputListener listener, int pointer)  {
		super(listener);
		this.pointer= pointer;
		this.button = 0;
		lastMove = new Vector2();
		offset  = new Vector2();
		
	}

	
	public void set(Vector2 coord, int button){
		this.set(coord.x,coord.y, button);
	}
	
	public void set(float x, float y, int button){
		super.set(x, y);
		lastMove.set(Vector2.Zero);
		offset.set(Vector2.Zero);
		timeStamp = TimeUtils.millis();
		touchTime = 0;
		this.button = button;
	}
	
	@Override
	public boolean doEvent(GuiElement e, float xRel, float yRel) {
	 return	e.onTouch(this,xRel,yRel);
	}
	
	public boolean doDrag(Vector2 newPos){

		if (target != null){
			temp.set(offset);
			offset.set(newPos.sub(coord));
			//System.out.println("offset:" + offset.x + "," + offset.y);
			lastMove.set(temp.sub(offset).scl(-1));
			//System.out.println("lastmove:" + lastMove.x + "," + lastMove.y);
			return target.onDrag(this);
		}
		return false;
	}

	public boolean doRelease() {
		touchTime = TimeUtils.timeSinceMillis(timeStamp);

		
		if (target != null){
			return target.onRelease(this);
		}
		return false;
	}
	
	public boolean doZoom(float start, float now) {
		if (target != null){
			if (prevDist == 0f){
				prevDist = start;
			}
			float ratio = prevDist / now;
			prevDist = now;			
			return target.onZoom(this, ratio);
		}
		this.endCapture();
		return false;
	}
	

	/**
	 * starts capturing gestures
	 */
	public void startCapture(){
		prevDist = 0f;
		this.listener.setCaptureEvent(this);
	}
	
	/**
	 * end gesture capture 
	 */
	public void endCapture(){
		this.listener.setCaptureEvent(null);
	}

}
