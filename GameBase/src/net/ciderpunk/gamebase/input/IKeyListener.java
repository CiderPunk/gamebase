package net.ciderpunk.gamebase.input;

public interface IKeyListener {
	boolean keyDown(int keyCode);
	boolean keyUp(int keyCode);
}
