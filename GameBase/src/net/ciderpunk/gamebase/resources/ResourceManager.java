package net.ciderpunk.gamebase.resources;

import java.util.Iterator;
import java.util.LinkedList;

import com.badlogic.gdx.utils.PooledLinkedList;
import net.ciderpunk.gamebase.gui.GameScreen;

import com.badlogic.gdx.assets.AssetManager;

public class ResourceManager  {

	AssetManager assetMan;
	PooledLinkedList<IResourceUser> postLoadList;
	boolean loading;
	GameScreen owner;


	public AssetManager getAssetMan() {
		return assetMan;
	}


	public void setAssetMan(AssetManager assetMan) {
		this.assetMan = assetMan;
	}


	public ResourceManager(GameScreen owner){
		assetMan = new AssetManager();
		postLoadList = new PooledLinkedList<IResourceUser>(100);
		this.owner = owner;	
		this.addResourceUser(owner);
	}
	
	
	public boolean update(){
		if (assetMan.update()) {
			if (loading){
				loading = false;
				postLoadList.iter();
				//Iterator<IResourceUser> i = postLoadList.iterator();
				IResourceUser item;
				while((item = postLoadList.next()) !=null){
					item.postLoad(this);
					postLoadList.remove();
				}
				owner.startGame();
				//we start proper next time!
				return false;
			}
			return true;
		}
		return false;
	}

	
	public void dispose(){
		assetMan.dispose();
	}
	
	public void addResourceUser(IResourceUser object){
		postLoadList.add(object);
		object.preLoad(this);

		loading = true;
	}
	
	public void addResourceUser(IResourceUser[] objects){
		for(IResourceUser object : objects){
			addResourceUser(object);
		}
	}

}
